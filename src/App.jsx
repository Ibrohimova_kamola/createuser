import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Navbar from './components/Navbar'
import 'bootstrap/dist/css/bootstrap.min.css'
import NewEventForm from './components/NewEventForm'
import { Modal } from 'bootstrap'

function App() {
  const [value, setValue] = useState(false)
  const [event, setEvent] = useState([])

  const newEvent = (event)=>{
    setEvent((prev)=>{
      return [...prev, event]
    })
  }
  const deleteItem = (id)=>{
    setEvent(event.filter((prev)=>{
      return prev.id !== id
    }))
  }
  return (
    <div className='container'>
      <Navbar />
      {value && 
      
       <NewEventForm newEvent={newEvent} setValue={setValue}/>}
     
      <button className='bg-sky-400' onClick={()=>{
        setValue(true)
      }}>Show Modal</button>
      {
        event.map((item) => {
          return (
            <div className="card" key={item.id}>
              <div className="card-header">
                <img src={item.img} alt="" />
              </div>
              <h2>Name: {item.name}</h2>
              <h2>Last Name: {item.lastName}</h2>
              <p>Job: {item.job}</p>
              <button onClick={()=> deleteItem(item.id)} className='btn btn-primary'>Delete</button>

            </div>

          )
        })
      }
    </div>
  )
}

export default App
