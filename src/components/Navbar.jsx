import React from 'react'
import './Navbar.css'

const Navbar = () => {
  return (
    <div className='container'>
        <nav className='d-flex justify-content-between align-items-center'>
            <h1 className='text-4xl font-semibold'>Logo</h1>
            <h3>No item :(</h3>
        </nav>
        <hr />
    </div>
  )
}

export default Navbar