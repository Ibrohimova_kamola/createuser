import React from 'react'
import { useState } from 'react'
import { v4 as uuidv4 } from 'uuid';

const NewEventForm = ({newEvent, setValue}) => {
    const [img, setImg] = useState('')
    const [name, setName] = useState('')
    const [lastName, setLastName] = useState('')
    const [job, setJob] = useState('')


    const resetValue = ()=>{
        setImg(''),
        setName(''),
        setLastName(''),
        setJob('')

    }
    const createCard = (e) => {
        e.preventDefault()
        console.log(e);

        const event = {
            id: uuidv4(),
            img: img,
            name:name,
            lastName: lastName,
            job: job
        }
        newEvent(event)
        console.log(event);
        resetValue()
        setValue(false)
    }
    return (
        <div className='d-flex justify-content-center'>
            <form className='card w-50 p-5'>
                <label>
                    <span>Img:</span>
                    <br />
                    <input onChange={(e) => setImg(e.target.value)} value={img} type="text" className='w-100 p-2' />
                </label>
                <label>
                    <span>Name:</span>
                    <br />
                    <input onChange={(e) => setName(e.target.value)} value={name} type="text" className='w-100 p-2' />
                </label>
                <label>
                    <span>Last Name:</span>
                    <br />
                    <input onChange={(e) => setLastName(e.target.value)} value={lastName} type="text" className='w-100 p-2' />
                </label>
                <label>
                    <span>Job:</span>
                    <br />
                    <input onChange={(e) => setJob(e.target.value)} value={job} type="text" className='w-100 p-2' />
                </label>
                <button onClick={createCard} className="btn btn-primary mt-3 py-3">Click me</button>

            </form>

        </div>
    )
}

export default NewEventForm